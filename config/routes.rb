Rails.application.routes.draw do
  root "dashboard#index"
  resources :human_triggers, only: [:index, :create]
end
