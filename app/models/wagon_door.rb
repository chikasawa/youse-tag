# frozen_string_literal: true

class WagonDoor < ApplicationRecord
  belongs_to :train_wagon
  has_one :beacon
end
