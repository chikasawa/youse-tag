# frozen_string_literal: true

class HumanTrigger < ApplicationRecord
  belongs_to :human
  belongs_to :beacon
end
