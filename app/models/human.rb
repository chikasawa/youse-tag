# frozen_string_literal: true

class Human < ApplicationRecord
  has_many :human_triggers
end
