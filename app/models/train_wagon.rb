# frozen_string_literal: true

class TrainWagon < ApplicationRecord
  belongs_to :train
  has_many :wagon_doors
end
