# frozen_string_literal: true

class Train < ApplicationRecord
  has_many :train_wagons, dependent: :destroy
end
