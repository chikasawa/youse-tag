# frozen_string_literal: true

class Beacon < ApplicationRecord
  belongs_to :wagon_door, optional: true
  belongs_to :human_trigger, optional: true
end
