import consumer from "./consumer"

consumer.subscriptions.create("DashboardChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
    console.log("Connected to the room!");
    $('#subway_initial').show();
    $('#subway_door_1').hide();
    $('#subway_door_2').hide();
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    // Called when there's incoming data on the websocket for this channel
    console.log("Recieving:")
    console.log(data.content)
    // $('#subway-image').html('<img src="/assets/metro_' + data.content.train.wagon.door.number + '.jpg">');
    $('#subway_initial').hide();
    if(data.content.train.wagon.door.number == 1){
      $('#subway_initial').hide();
      $('#subway_door_1').show();
      $('#subway_door_2').hide();
    } else {
      $('#subway_initial').hide();
      $('#subway_door_1').hide();
      $('#subway_door_2').show();
    }

    setTimeout(
      function(){
        $('#subway_initial').show();
        $('#subway_door_1').hide();
        $('#subway_door_2').hide();
      }, 5000);

    $('#human_triggers').prepend('<tr class="border-b hover:bg-orange-100 bg-gray-100">' +
                    '<td class="p-3 px-5"><img class="rounded-full" width="50" height="50" src="' + data.content.human.avatar_url + '"></td>' +
                    '<td class="p-3 px-5">' + data.content.human.name + '</td>' +
                    '<td class="p-3 px-5">' + data.content.train.name + '</td>' +
                    '<td class="p-3 px-5">' + data.content.train.wagon.number + '</td>' +
                    '<td class="p-3 px-5">' + data.content.train.wagon.door.number + '</td>' +
                    '<td class="p-3 px-5">' + data.content.created_at + '</td>' +
                '</tr>');

    // $('tbody').prepend(a.append($('<td/>').html('teste')))


    // $('#user-image').html('<img src="/assets/' + data.content.human.id + '.jpeg">');
    // $('#msg').append('<div class="message"> ' + data.content.human.name +
    //   ' está solicitando auxílio no trem ' + data.content.train.number +
    //   ', no vagão nº' + data.content.train.wagon.number +
    //   ', na porta nº' + data.content.train.wagon.door.number + '</div>')
  }
});
