# frozen_string_literal: true

class HumanTriggerSerializer < ActiveModel::Serializer
  attribute :reason
  attribute :created_at do
    object.created_at.strftime("%d/%m/%Y %H:%M:%S")
  end
  attribute :human
  attribute :train

  def human
    {
      id: object.human.id,
      name: object.human.name,
      mail: object.human.mail,
      document_number: object.human.document_number,
      avatar_url: object.human.avatar_url
    }
  end

  def train
    door = object.beacon.wagon_door
    wagon = door.train_wagon
    train = wagon.train

    {
      id: train.id,
      name: train.name,
      number: train.number,
      wagon: {
        number: wagon.number,
        door: {
          number: door.number
        }
      }
    }
  end
end
