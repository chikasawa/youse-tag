class DashboardController < ApplicationController
  def index
    @human_triggers = HumanTrigger.order('created_at DESC').all
  end
end
