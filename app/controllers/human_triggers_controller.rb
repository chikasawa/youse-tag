class HumanTriggersController < ApplicationControllerAPI
  def index
    triggers = HumanTrigger.order('created_at DESC').all
    render json: triggers, each_serializer: HumanTriggerSerializer
  end

  def create
    beacon = Beacon.find_by_number(permitted_params[:beacon_number].downcase)
    human = Human.find(permitted_params[:human_uid])

    human_trigger = HumanTrigger.create({
      human_id: human.id,
      beacon_id: beacon.id,
      reason: 'Train Emergency'})

    ActionCable.server.broadcast('dashboard_channel', content: HumanTriggerSerializer.new(human_trigger).as_json)

    render json: human_trigger
  end

  private

  def permitted_params
    params.require(:human_uid)
    params.require(:beacon_number)

    params
  end
end
