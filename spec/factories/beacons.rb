FactoryBot.define do
  factory :beacon do
    name { Faker::Alphanumeric.unique.alphanumeric(number: 10) }
    number { Faker::Number.unique.number(digits: 5)  }
    active { true }
  end
end
