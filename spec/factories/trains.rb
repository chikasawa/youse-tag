FactoryBot.define do
  factory :train do
    name { Faker::Alphanumeric.unique.alphanumeric(number: 10) }
    number { Faker::Number.unique.number(digits: 5)  }
    train_wagons { build_list(:train_wagon, 6) }
  end
end
