FactoryBot.define do
  sequence :door_number do |number|
    generated = number % 8

    generated.zero? ? 8 : generated
  end

  factory :wagon_door do
    name { Faker::Alphanumeric.unique.alphanumeric(number: 10) }
    number { generate(:door_number) }
    beacon { association(:beacon) }
  end
end
