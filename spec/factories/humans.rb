FactoryBot.define do
  factory :human do
    name { Faker::Name.unique.name  }
    mail { Faker::Internet.unique.email }
    document_number {  Faker::IDNumber.unique.brazilian_citizen_number(formatted: true)}
  end
end
