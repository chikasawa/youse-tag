FactoryBot.define do
  sequence :wagon_number do |number|
    generated = number % 6

    generated.zero? ? 6 : generated
  end

  factory :train_wagon do
    name { Faker::Alphanumeric.unique.alphanumeric(number: 10) }
    number { generate(:wagon_number)  }
    wagon_doors { build_list(:wagon_door, 8) }
  end
end
