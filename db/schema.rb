# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_20_171256) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "beacons", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "wagon_door_id"
    t.string "number"
    t.string "name"
    t.boolean "active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "human_trigger_id"
    t.index ["human_trigger_id"], name: "index_beacons_on_human_trigger_id"
    t.index ["wagon_door_id"], name: "index_beacons_on_wagon_door_id"
  end

  create_table "human_triggers", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "human_id"
    t.uuid "beacon_id"
    t.string "reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["beacon_id"], name: "index_human_triggers_on_beacon_id"
    t.index ["human_id"], name: "index_human_triggers_on_human_id"
  end

  create_table "humans", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "name"
    t.string "mail"
    t.string "document_number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "avatar_url"
  end

  create_table "train_wagons", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "train_id"
    t.string "name"
    t.integer "number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["train_id"], name: "index_train_wagons_on_train_id"
  end

  create_table "trains", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "number"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["number"], name: "index_trains_on_number", unique: true
  end

  create_table "wagon_doors", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "train_wagon_id"
    t.string "name"
    t.integer "number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["train_wagon_id"], name: "index_wagon_doors_on_train_wagon_id"
  end

  add_foreign_key "beacons", "human_triggers", on_delete: :cascade
  add_foreign_key "beacons", "wagon_doors", on_delete: :cascade
  add_foreign_key "human_triggers", "beacons"
  add_foreign_key "human_triggers", "humans"
  add_foreign_key "train_wagons", "trains", on_delete: :cascade
  add_foreign_key "wagon_doors", "train_wagons", on_delete: :cascade
end
