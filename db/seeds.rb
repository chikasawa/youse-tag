# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts " Creating data ".center(100, '*')

FactoryBot.create_list(:train, 10) if Train.count.zero?

people = [
  { name: 'Camila Rodriges', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-U726X4VP0-f1487e4207e4-512' },
  { name: 'Bruno Vicenzo', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-U0SR7NF1D-6a41f09a2b92-512' },
  { name: 'Celso Fujii', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-UNZ935TDH-e4d9a3678b41-512' },
  { name: 'Daniel Coca', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-U0ZV63BUZ-b6339704ce33-512' },
  { name: 'Robson Chikasawa', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-U0DPDQ4AV-1a570e7be223-512' },
  { name: 'Ricardo Neves', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-UFA05D9K9-162168e7f43a-512' },
  { name: 'Fabio Temistocle', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-U4YFAP5F0-afdf74dd6cde-512' },
  { name: 'Thiago Delmotte', avatar_url: 'https://ca.slack-edge.com/T0AB16SBH-UNSRKK056-e44143f35cd4-512' }
]

people.each { |person| FactoryBot.create(:human, name: person[:name]) } if Human.count.zero?

people.each { |person| Human.find_by(name: person[:name]).update(avatar_url: person[:avatar_url]) }

puts " Associating beacons numbers ".center(100, '*')

train = Train.first

wagon = train.train_wagons.order(:number).second

doors = wagon.wagon_doors.order(:number).first(4)

coca_beacon = '4429d3ce-7975-4818-ae4d-5d4b06caa034'
doors.first.beacon.update(number: coca_beacon) unless Beacon.where(number: coca_beacon)

chika_beacon = 'b9407f30-f5f8-466e-aff9-25556b57fe6d'

door_2 = doors.find {|door| door.number == 2 }

door_chika = doors.find(-> { doors.last }) { |door| door.beacon.number == chika_beacon }

door_2.number = door_chika.number
door_chika.beacon.number = chika_beacon unless door_chika.beacon.number == chika_beacon
door_chika.number = 2

[door_2, door_chika].each(&:save!)

puts " Finished ".center(100, '*')
