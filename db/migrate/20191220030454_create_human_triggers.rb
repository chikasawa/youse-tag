class CreateHumanTriggers < ActiveRecord::Migration[6.0]
  def change
    create_table :human_triggers, id: :uuid, default: 'uuid_generate_v4()'  do |t|
      t.references :human, foreign_key: true, type: :uuid
      t.references :beacon, foreign_key: true, type: :uuid
      t.string :reason

      t.timestamps
    end
  end
end
