class AddAvatarUrlToHuman < ActiveRecord::Migration[6.0]
  def change
    add_column :humans, :avatar_url, :string
  end
end
