class CreateBeacons < ActiveRecord::Migration[6.0]
  def change
    create_table :beacons, id: :uuid, default: 'uuid_generate_v4()'  do |t|
      t.references :wagon_door, foreign_key: { on_delete: :cascade }, type: :uuid
      t.string :number
      t.string :name
      t.boolean :active

      t.timestamps
    end
  end
end
