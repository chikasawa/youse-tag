class CreateHumans < ActiveRecord::Migration[6.0]
  def change
    create_table :humans, id: :uuid, default: 'uuid_generate_v4()'  do |t|
      t.string :name
      t.string :mail
      t.string :document_number

      t.timestamps
    end
  end
end
