class AddHumanTriggerToBeacon < ActiveRecord::Migration[6.0]
  def change
    add_reference :beacons, :human_trigger, foreign_key: { on_delete: :cascade }, type: :uuid
  end
end
