class CreateTrains < ActiveRecord::Migration[6.0]
  def change
    create_table :trains, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.string :number
      t.string :name

      t.timestamps
    end

    add_index :trains, :number, unique: true, using: :btree
  end
end
