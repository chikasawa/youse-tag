class CreateWagonDoors < ActiveRecord::Migration[6.0]
  def change
    create_table :wagon_doors, id: :uuid, default: 'uuid_generate_v4()'  do |t|
      t.references :train_wagon, foreign_key: { on_delete: :cascade }, type: :uuid
      t.string :name
      t.integer :number

      t.timestamps
    end
  end
end
